const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const base = require('./webpack.base.config')
const nodeExternals = require('webpack-node-externals')
const VueSSRServerPlugin = require('vue-server-renderer/server-plugin')
const isProd = process.env.NODE_ENV === 'production'
const config = require('./config');
const env = (isProd)?"prod":"dev";
const lo = require('lodash');

console.log('OUTPUT',config.root,config.ssrOutput);

module.exports = merge(base, {
  target: 'node',
  devtool: '#source-map',
  entry: config.indexSsr,
  output: {
    path:config.root,
    filename: config.ssrOutput,
    libraryTarget: 'commonjs2'
  },
  resolve: {
    extensions: ['.js', '.vue', '.css', '.json','.less'],
    alias: {
      "node_modules": path.join(__dirname,'../../node_modules')
    }
  },
  // https://webpack.js.org/configuration/externals/#externals
  // https://github.com/liady/webpack-node-externals
  externals: nodeExternals({
    // do not externalize CSS files in case we need to import it from a dep
    whitelist: /\.css$/,
    modulesDir: path.join(__dirname, '../../node_modules')
  }),
  plugins: [
    new webpack.DefinePlugin(lo.merge({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
      'process.env.VUE_ENV': '"server"'
    },config[env].clientVars)),
    new VueSSRServerPlugin()
  ]
})
