const path = require('path');
const webpack = require('webpack')
const vueConfig = require('./vue-loader.config')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const config = require('./config');
const isProd = process.env.NODE_ENV === 'production'
const env = (isProd)?"prod":"dev";

module.paths.push(__dirname + '/node_modules');

module.exports = {
  devtool: isProd
    ? false
    : '#cheap-module-source-map',
  output: {
    path: config[env].dist,
    publicPath: config.publicPath,
    filename: '[name].[chunkhash].js'
  },
  resolve: {
    alias: {
      'src': path.resolve(config.src),
      //'node_modules':,path.resolve(__dirname, 'node_modules')
      'public': path.join(config.root,'public'),
      'node-sass' : path.resolve(__dirname, 'node_modules/node-sass')
    },
    modules: [path.resolve(__dirname, 'node_modules'), 'node_modules'],

  },
  resolveLoader: {
    modules: ['node_modules',path.resolve(__dirname, 'node_modules')],
    extensions: ['.js', '.json'],
    mainFields: ['loader', 'main']
  },
  module: {
    noParse: /es6-promise\.js$/, // avoid webpack shimming process
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: vueConfig
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: '[name].[ext]?[hash]'
        }
      },
      {
        test: /\.css$/,
        use: isProd
          ? ExtractTextPlugin.extract({
            use: [ {loader: "css-loader?minimze"},
                {loader: "less-loader", options: {
                modifyVars: {
                  "@theme":process.env.NODE_ENV
                }
              }
            }],
              fallback: 'vue-style-loader'
            })
          : ['vue-style-loader',  'css-loader']
      },
      {
        test: /\.styl(us)?$/,
        use: isProd
          ? ExtractTextPlugin.extract({
              use: [
                {
                  loader: 'css-loader',
                  options: { minimize: true }
                },
                'stylus-loader'
              ],
              fallback: 'vue-style-loader'
            })
          : ['vue-style-loader', 'css-loader', 'stylus-loader']
      },
    ]
  },
  performance: {
    maxEntrypointSize: 300000,
    hints: isProd ? 'warning' : false
  },
  plugins: isProd
    ? [
        new webpack.optimize.UglifyJsPlugin({
          compress: { warnings: false }
        }),
        new ExtractTextPlugin({
          filename: 'common.[chunkhash].css'
        })
      ]
    : [
        new FriendlyErrorsPlugin()
      ]
}
