const webpack = require('webpack')
const merge = require('webpack-merge')
const base = require('./webpack.base.config')
const SWPrecachePlugin = require('sw-precache-webpack-plugin')
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin')
const isProd = process.env.NODE_ENV === 'production'
const config = require('./config');
const env = (isProd)?"prod":"dev";
const lo = require('lodash');
const FriendlyErrors = require('friendly-errors-webpack-plugin')

const wpConfig = merge(base, {
  entry: {
    app: config.dev.js
  },
  resolve: {
    extensions: ['.js', '.vue', '.json','.less'],
    alias: {
      'create-api': './create-api-client.js'
    }
  },
  plugins: [
    // strip dev-only code in Vue source
    new webpack.DefinePlugin(lo.merge({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
      'process.env.VUE_ENV': '"client"'
    },config[env].clientVars)),
    // extract vendor chunks for better caching
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module) {

        return (
          // it's inside node_modules
          /node_modules/.test(module.context) &&
          !/\.css$/.test(module.request)
        )
      }
    }),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest'
    }),
    new VueSSRClientPlugin()
  ]
})

if (process.env.NODE_ENV === 'production') {
  wpConfig.plugins.push(
    // auto generate service worker
    new SWPrecachePlugin(lo.merge({
      cacheId: 'vue-hn',
      filename: 'service-worker.js',
      dontCacheBustUrlsMatching: /./,
      minify:true,
      staticFileGlobsIgnorePatterns: [/\.map$/, /\.json$/],
      runtimeCaching: [
        {
          urlPattern: '/',
          handler: 'networkFirst'
        },]
    },config.pwaConfig))
  )
}
else {
 // wpConfig.plugins.push(new webpack.HotModuleReplacementPlugin());
  wpConfig.plugins.push(new webpack.NoEmitOnErrorsPlugin());
  wpConfig.plugins.push(new FriendlyErrors());
}

module.exports = wpConfig
