const pkg = require('./package'),
	path = require('path'),
	fs = require('fs'),
	_ = require('lodash');

const base = path.join(__dirname, '../../');
var edit;
let configFile = path.join(base, 'tpl.js');
if (fs.existsSync(configFile)) {
	edit = require(configFile);
}

var config = {
	port: 4000,
	title: 'tpl no title',
	API: '',
	root: base,
	src: path.join(base, 'src'),
	static: 'static',
	ssrOutput: 'ssr-bundle.js',
	dev: {
		ssrPort: 3000,
		dist: path.join(base, 'dev'),
		clientVars: {}
	},
	prod: {
		ssrPort: 9889,
		dist: path.join(base, 'dist'),
		clientVars: {}
	}
}

config.indexSsr = path.join(config.src, 'entry-server.js');
config.publicPath = '/'+config.static;

if (typeof edit == 'object') {
	_.merge(config, edit)
}

_.merge(config.dev, {
	static: path.join(config.src, 'static'),
	js: path.join(config.src,'/entry-client.js'),
	mainLess: path.join(config.src, 'less', 'main.less'),
})
_.merge(config.prod, {
	//...config['prod'],
	static: path.join(config.prod.dist, 'static'),
	index: path.join(config.prod.dist, 'index.html'),
	js: path.join(config.prod.dist, 'static', 'build.js'),
})

module.exports = config