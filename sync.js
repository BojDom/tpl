const mst = require('mobx-state-tree');
const {Subject} = require('rxjs');
const {bufferTime} = require('rxjs/operators');
const {throttle,get} = require('lodash');


class server {

    constructor(scServer){
        this.data = {};
        this.subs = {};
        this.patch = {};
        this.patchSubs = {}
        scServer.addMiddleware(scServer.MIDDLEWARE_PUBLISH_IN,function(req, next) {
				
                let splitted = req.channel.split('/');
                if (splitted[0]&&splitted[0]=='priv'){
                    if (splitted[1]&&splitted[1]==req.socket.authToken._id) next();
                    else next('not.auth')
                }
                else {
                    req.data.clientId = req.socket.authToken._id
                    next()
                }
            }
        );
        scServer.addMiddleware(scServer.MIDDLEWARE_SUBSCRIBE,function(req, next) {
                let splitted = req.channel.split('/');
                if (splitted[0]&&splitted[0]=='priv'){
                    if (splitted[1]&&splitted[1]==req.socket.authToken._id) next();
                    else next('not.auth')
                }
                else 
                next()
        });
        this.scServer = scServer;
    }
    sync({channel,mstStructure,initialData,options}){
        
        this.data[channel] = mstStructure.create(initialData);
        this.patch[channel] = new Subject();
        this.patchSubs[channel].pipe(bufferTime(100),filter(a=>a.length)).subscribe(a=>{
            this.scServer.exchange.publish(`${channel}/patch`,a);
        });

        mst.onPatch(this.data[channel],d=>{this.patchSubs[channel].next(d)});
        if (get(options,'onSnapshot'))
            mst.onSnapshot(this.data[channel],throttle( data=>{
                options.onSnapshot(data)
            },1000 ))

        this.subs[ channel + '_snap' ] = this.scServer.exchange.subscribe(channel + '/getSnapshot');
        this.subs[ channel + '_snap' ].watch(d=>{
            this.scServer.exchange.publish(`priv/${d.clientId}/snap`,mst.getSnapshot( this.data[channel] ))
        })
    }
}

class client {
   

    constructor(socket){
        this.subs= {}
        this.socket = socket;
    }
    async sync({channel,options}){
        //if (socket.)
        this.socket.publish(channel + '/getSnapshot');
        this.subs[channel+'_snap']= this.socket.subscribe(`priv/${socket.authToken._id}/snap`);
        this.subs[channel+'_snap'].watch(data=>{
            options.onSnapshot(data)
        })
    }
}

module.exports.server = server
module.exports.client = client